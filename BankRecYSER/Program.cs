﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using BankRecYSLIB;
using YSUtilities;
using System.Data;
using System.Linq;

namespace BankRecYSER
{
    class Program
    {
        private const string SQL_STMT_UPDATE =
            "update HS_PROCESSING " +
            "set Status = '{0}', " +
            "CompleteTime = '{1}', " +
            "CompletionMessage = '{2}' " +
            "where ID = {3} ";

        const string LOG_FILE_NAME = @"C:\HillMgt Automation\HillEnterpriseReporting\BankRecER\BankRecWeb.log";
        const string CONNECTION_STRING = @"data source=HMFINSQL;initial catalog={0};Integrated Security=true;";
        const string EXCEL_OUTPUT_PATH = @"C:\HillMgt Automation\HillEnterpriseReporting\BankRecYSER";
        private static string rowId = string.Empty;
        private static string connString = string.Empty;
        public static YSLogger logger = null;
        public static string CurrentUserEmail = string.Empty;
        public static bool SomePropsNotIncluded = false;
        public static string queryPeriod = string.Empty;
        public static YSSqlAgentSQL sa = null;
        private static string selectionMethod = string.Empty;

        // This dictioinary contains all properties with columns in the report.
        // Also a flag indicating whether or not to include said property in the report.
        public static Dictionary<string, bool> IncludedProperties = new Dictionary<string, bool>()
        {
            {"049", false }, {"051", false}, {"067", false }, {"068", false }, {"071", false },
            {"073", false }, {"077", false }, {"078", false }, {"087", false }
        };

        static int Main(string[] args)
        {
            List<string> msgs = new List<string>();
            const string LOG_FILE_NAME = @"C:\HillMgt Automation\HillEnterpriseReporting\BankRecYSER\BankRecYSER.log";
            logger = new YSLogger(LOG_FILE_NAME);

            BankRes_PageInfo PageInfo = new BankRes_PageInfo();

            if (args.Length != 2)
            {
                logger.LogMessage("Invalid number of arguments passed.  Requires 2: DATABASE, and RowId");
                return (-1);
            }
            
            connString = string.Format(CONNECTION_STRING, args[0]);
            rowId = args[1];

            ReportDBAgent ra = new ReportDBAgent(logger, connString);

            try
            {
                // Fetching actual parameters from database
                sa = new YSSqlAgentSQL(logger);
                DataSet ds = ra.GetSpecificRow(rowId);
                ra.SetInProgress(rowId);

                string parms = ds.Tables[0].Rows[0]["Parms"].ToString().Trim();
                string userId = ds.Tables[0].Rows[0]["UserId"].ToString();
                CurrentUserEmail = ds.Tables[0].Rows[0]["Email"].ToString();
                logger.LogMessage(string.Format("Current User's Email: {0}", CurrentUserEmail));

                queryPeriod = ParseMRIParameters(parms);
                logger.LogMessage(string.Format("Running for Query Period: {0}", queryPeriod));

                // Setup first day and last day of month based on queryPeriod
                PageInfo.FirstDay = new DateTime(
                    int.Parse(queryPeriod.Substring(0, 4)),
                    int.Parse(queryPeriod.Substring(4, 2)), 1);

                // Add one to the prior month, the subtract 1 day to get the last day of the month
                PageInfo.LastDay = PageInfo.FirstDay.AddMonths(+1).AddDays(-1);

                PageInfo.OutputDirectory = @"C:\HillMgt Automation\HillEnterpriseReporting\BankRecYSER";
                PageInfo.ExcelInputFileName = string.Format("{0}\\BankRec_Yourspace_Blank.xls", PageInfo.OutputDirectory);

                PageInfo.ExcelOutputFileName = string.Format("{0}\\{1}.xls", PageInfo.OutputDirectory, Guid.NewGuid().ToString());

                logger = new YSLogger(LOG_FILE_NAME);
                YSExcelAgent ea = new YSExcelAgent(logger);

                logger.LogMessage("BankRec_Peak_YourSpace_Started");

                // Perform file maintenance on all XLS files except for the Inventory
                // report and the spreadsheet blank.
                string[] files = Directory.GetFiles(PageInfo.OutputDirectory, "*.xls");
                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.Name != "BankRec_Yourspace_Blank.xls")
                    {
                        logger.LogMessage(string.Format("Deleting file '{0}' as part of routine maintenance", file));
                        File.Delete(file);
                    }
                }

                // This is the main line of the program.
                // Run each sheet in sequence
                BankRec_YS_Lockbox lb = new BankRec_YS_Lockbox(logger, PageInfo, ea);
                lb.Process(IncludedProperties, connString.Trim());
                logger.LogMessage("--------------------------------------------------------------------------------------------");

                BankRec_YS_ACH ach = new BankRec_YS_ACH(logger, PageInfo, ea);
                ach.Process(IncludedProperties, connString.Trim());
                logger.LogMessage("--------------------------------------------------------------------------------------------");

                BankRec_YS_Reject rej = new BankRec_YS_Reject(logger, PageInfo, ea);
                rej.Process(IncludedProperties, connString.Trim());
                logger.LogMessage("--------------------------------------------------------------------------------------------");

                BankRec_YS_Field fld = new BankRec_YS_Field(logger, PageInfo, ea);
                fld.Process(IncludedProperties, connString.Trim());
                logger.LogMessage("--------------------------------------------------------------------------------------------");

                // Save the file 
                PageInfo.ExcelOutputFileName= string.Format("{1}\\BankRecYS_{0}.xlsx", selectionMethod, EXCEL_OUTPUT_PATH);
                ea.SaveAndClose(PageInfo.ExcelOutputFileName);

                // Send Email
                YSEmailAgent ema = new YSEmailAgent(logger);
                // Only send the email if an attachment has been created.
                logger.LogMessage("About to send email.");
                if (File.Exists(PageInfo.ExcelOutputFileName))
                    ema.SendEmail(PageInfo.ExcelOutputFileName, CurrentUserEmail);
                logger.LogMessage("Email sent.");

                sa = new YSSqlAgentSQL(logger);
                ra.SetComplete(rowId);
                return (0);
            }
            catch (Exception _ex)
            {
                sa = new YSSqlAgentSQL(logger);
                ra.SetFailed(_ex.Message, rowId);
                logger.LogMessage(string.Format("Error occured in Program: {0}", _ex.Message));
                return (-1);
            }
            finally
            {
                //Console.WriteLine("Press any key...");
                //Console.ReadLine();
            }
        }

        public static string ParseMRIParameters(string mriInClause)
        {
            // excelInClause is the in clause generated in the function above.  It reads ALL properties
            // from the blank Excel worksheet and formulates a collection of SquareFootAndOccupancy objects.

            // mriInClause is a string containing an array of propertyIds delimited by pipes.
            // The first item in this list is an indicator of what this list of properties is for.
            // The first character will always be:  I (Include), A (All), E (Exclude), or R (Range)
            // The second element, BLDG, indicates which DB Table the information came from.  In
            // this case the BLDG table.
            // The remaining elements  are  property ids

            // I | BLDG | 101 | 102 | 103 |
            //     Run program only for properties 101, 102 and 103

            // A
            //     Run program for all properties

            // E | BLDG | 301 | 302 | 303 |
            //     Run program for all properties EXCEPT 301, 302 and 303

            // R | BLDG | 101 | 106 |
            //     Run program for all properties BETWEEN 101 and 106

            string retVal = string.Empty;
            if (mriInClause.Substring(mriInClause.Length - 1, 1) == "|")
                mriInClause = mriInClause.Substring(0, mriInClause.Length - 1);

            string[] properties = mriInClause.Split('|');
            retVal = properties[0]; // First item of this array is the queryPeriod.  Return that.

            // Bankrec will always print ALL properties, regardless.  I'm forcing this
            // without destroying this working code.  (In case -- and when -- the inevitable occurrs.)
            //switch (properties[1].ToUpper())      // (Original) - this is the indicator above: I, A, E or R
            switch ("A")      
            {
                // In the case of A, meaning ALL, simply return the original
                // inClause and turn all sfao ProcessIndicators to true;
                case "A":
                    Dictionary<string, bool> tempA = new Dictionary<string, bool>();
                    foreach (KeyValuePair<string, bool> kvp in IncludedProperties)
                        tempA.Add(kvp.Key, kvp.Value);

                    foreach (KeyValuePair<string, bool> kvp in tempA)
                        IncludedProperties[kvp.Key] = true;
                    selectionMethod = "All";

                    break;

                // In the case of I, meaning Include, parse the propety ids into
                // a new in clause and trun ONLY those sfao's included in the 
                // list ProcessIndicators to true;
                case "I":
                    for (int i = 3; i < properties.Count(); i++)
                        if (IncludedProperties.ContainsKey(properties[i]))
                            IncludedProperties[properties[i]] = true;
                        else
                            SomePropsNotIncluded = true;
                    selectionMethod = "Include";

                    break;

                // In the case of E, meaning Exclude, turn all ProcessIndicators
                // on, then turn off the ones corresponding the the props in the list.
                case "E":
                    // First change all process indicators to true.
                    // then turn off the exclusions.
                    Dictionary<string, bool> tempE = new Dictionary<string, bool>();
                    foreach (KeyValuePair<string, bool> kvp in IncludedProperties)
                        tempE.Add(kvp.Key, kvp.Value);

                    foreach (KeyValuePair<string, bool> kvp in tempE)
                        IncludedProperties[kvp.Key] = true; // kvp.Value is a sfao.

                    for (int i = 3; i < properties.Count(); i++)
                        if (IncludedProperties.ContainsKey(properties[i]))
                            IncludedProperties[properties[i]] = false;
                        else
                            SomePropsNotIncluded = true;
                    selectionMethod = "Exclude";

                    break;

                // In the case of R, meaning Range, turn on all process indicators
                // where the property ids are BETWEEN  the first prop and second prop
                case "R":
                    int min = int.Parse(properties[3]);
                    int max = int.Parse(properties[4]);

                    Dictionary<string, bool> tempR = new Dictionary<string, bool>();
                    foreach (KeyValuePair<string, bool> kvp in IncludedProperties)
                        tempR.Add(kvp.Key, kvp.Value);

                    foreach (KeyValuePair<string, bool> kvp in tempR)
                    {
                        if (int.Parse(kvp.Key) >= min && int.Parse(kvp.Key) <= max)
                            IncludedProperties[kvp.Key] = true;
                    }
                    selectionMethod = "Range";

                    break;
            }


            return retVal;
        }
    }
}
