﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YSUtilities;

namespace BankRecYSLIB
{
    public class BankRec_YS_Field : BankRes_YS_Base
    {
        private float[] cws = { 11.5F, 11.5F, 11.5F, 11.5F, 11.5F, 11.5F, 11.5F, 11.5F, 11.5F, 11.5F };    // Set the column widths
        private const string SQL_STMT = "SELECT rdate " +
        "    , sum(s049) AS S049 " +
        "    , sum(s051) AS S051 " +
        "    , sum(s067) AS S067 " +
        "    , sum(s068) AS S068 " +
        "    , sum(S071) AS S071 " +
        "    , sum(S073) AS S073 " +
        "    , SUM(S077) AS S077 " +
        "    , sum(S078) AS S078 " +
        "    , sum(S087) AS S087 " +
    "FROM( " +
    "SELECT cast(receiptdat AS DATE) AS rdate " +
    "    , sum(CASE WHEN siteid = 49 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S049 " +
    "    , sum(CASE WHEN siteid = 51 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S051 " +
    "    , sum(CASE WHEN siteid = 67 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S067 " +
    "    , sum(CASE WHEN siteid = 68 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S068 " +
    "    , sum(CASE WHEN siteid = 71 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S071 " +
    "    , sum(CASE WHEN siteid = 73 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S073 " +
    "    , sum(CASE WHEN siteid = 77 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S077 " +
    "    , sum(CASE WHEN siteid = 78 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S078 " +
    "    , sum(CASE WHEN siteid = 87 THEN PMT_CHECK + PMT_CASH " +
    "            ELSE 0 END) AS S087 " +
    "FROM HS_SLRECEIPTS " +
    "WHERE cast(RECEIPTDAT AS DATE) >= '{0}' " +
    "	AND cast(RECEIPTDAT AS DATE) <= '{1}' " +
    "	AND sby not in ('aa') and sby not like '%@%' " +
    "GROUP BY cast(receiptdat AS DATE) " +
    "	,siteid " +
    ") AS t1 " +
    "GROUP BY rdate order by RDATE";

        public BankRec_YS_Field(YSLogger logger, BankRes_PageInfo pi, YSExcelAgent ea) : base(logger, pi, ea)
        {
            logger.LogMessage("==================== Field ====================");
            PageInfo.PageName = "Field Deposits";
            PageInfo.TotalLineText = "Field Totals";
            PageInfo.SheetName = "Field";
            PageInfo.FirstDataRow = 5;

            mySqlStatement = string.Format(SQL_STMT,
                PageInfo.FirstDay.ToShortDateString(),
                PageInfo.LastDay.ToShortDateString());
        }

        public override void Process(Dictionary<string, bool> IncludedProperties, string conn)
        {
            base.Process(IncludedProperties, conn.Trim());
        }
    }
}
