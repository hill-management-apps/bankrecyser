﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using YSUtilities;

namespace BankRecYSLIB
{
    public class BankRes_YS_Base
    {
        protected BankRes_PageInfo PageInfo = null;
        protected YSLogger logger;
        protected YSExcelAgent excelAgent = null;
        protected DataSet myData = null;
        protected string mySqlStatement = string.Empty;

        protected BankRes_YS_Base(YSLogger sfLogger, BankRes_PageInfo pi, YSExcelAgent ea)
        {
            logger = sfLogger;
            PageInfo = pi;
            excelAgent = ea;
            PageInfo.ReportName = "Peak Management LLC - YourSpace storageLC";
            PageInfo.DataHeaderRow = 7;
            PageInfo.ColumnHeaderRow = 5;
        }

        public virtual void Process(Dictionary<string, bool> IncludedProperties, string conn)
        {
            excelAgent.ApplyInitialFormat(PageInfo);
            YSSqlAgentSQL sa = new YSSqlAgentSQL(logger);

            #region Fetch the Data From SQL Server

            // ONLY DO THIS IF WE ARE *NOT* ON THE TOTALS PAGE !!!!!

            // Execute the SQL statement and set the SqlData property
            logger.LogMessage("Executing SQL");
            try
            {
                sa = new YSSqlAgentSQL(logger);
                myData = sa.ExecuteQuery(mySqlStatement, conn.Trim());

                excelAgent.LoadDataRows(myData, IncludedProperties);
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in BankRes_YS_Base.Process: {0}", _ex.Message));
                throw _ex;
            }

            #endregion
        }
    }
}
