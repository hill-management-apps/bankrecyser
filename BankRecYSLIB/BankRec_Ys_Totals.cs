﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YSUtilities;

namespace BankRecYSLIB
{
    public class BankRec_Ys_Totals : BankRes_YS_Base
    {
        private float[] cws = { 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 19F, 14F };    // Set the column widths

        public BankRec_Ys_Totals(YSLogger logger, BankRes_PageInfo pi, YSExcelAgent ea) : base(logger, pi, ea)
        {
            logger.LogMessage("==================== Totals ====================");
            PageInfo.PageName = "Cash Reciept Summary";
            PageInfo.TotalLineText = string.Empty;
            PageInfo.SheetName = "Totals";
            columnWidths = cws.ToList();
        }

        public override void Process()
        {
            base.Process();
        }
    }
}
