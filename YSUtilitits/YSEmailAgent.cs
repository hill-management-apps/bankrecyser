﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YSUtilities;
using System.Net.Mail;

namespace YSUtilities
{
    public class YSEmailAgent
    {
        private const string AS400_SMTP_SERVER = "vader.hsales.com";
        private const string OFFICE365_SMTP_SERVER = "Smtp.office365.com";

        private const string FROM_EMAIL_ADDR = "donotreply@peakmgt.com";
        private const string EMAIL_SUBJECT = "BankRec YourSpace - Rent Roll Spreadsheet Attached";
        private const string EMAIL_BODY = "Attached, please find an Excel Spreadsheet.";

        YSLogger logger;

        public YSEmailAgent(YSLogger sfLogger)
        {
            logger = sfLogger;
        }

        public void SendEmail(string attachment, string userEmail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(OFFICE365_SMTP_SERVER);

                mail.From = new MailAddress(FROM_EMAIL_ADDR);
                mail.To.Add(userEmail);

                mail.Subject = EMAIL_SUBJECT;
                mail.Body = EMAIL_BODY;

                if (attachment.Length > 0)
                {
                    Attachment a = new Attachment(attachment);
                    mail.Attachments.Add(a);
                }

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("donotreply@peakmgt.com", "Pwdnr593#");      // TODO
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }
    }
}
