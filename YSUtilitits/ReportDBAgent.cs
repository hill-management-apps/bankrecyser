﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YSUtilities
{
    public class ReportDBAgent
    {
        YSLogger logger;
        string connectionString = string.Empty;

        private const string SQL_STMT_UPDATE =
            "update HS_PROCESSING " +
            "set Status = '{0}', " +
            "CompleteTime = '{1}', " +
            "CompletionMessage = '{2}' " +
            "where ID = {3} ";


        public ReportDBAgent(YSLogger _logger, string connStr)
        {
            logger = _logger;
            connectionString = connStr;
            logger.LogMessage("Here in ReportDBAgent ctor");
        }

        public DataSet GetSpecificRow(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.GetSpecificRow()");
            YSSqlAgentSQL sa = new YSSqlAgentSQL(logger);
            return sa.ExecuteQuery(string.Format("Select * from HS_PROCESSING where id = {0}", rowId), connectionString);
        }

        public void SetInProgress(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetInProgress()");
            YSSqlAgentSQL sa = new YSSqlAgentSQL(logger);

            string sqlU = string.Format(SQL_STMT_UPDATE, "In Progess",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Empty, rowId);
            
            sa.ExecuteNonQuery(sqlU, connectionString);
        }

        public void SetFailed(string msg, string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetFailed()");
            YSSqlAgentSQL sa = new YSSqlAgentSQL(logger);

            string sqlF = string.Format(SQL_STMT_UPDATE, "Failed",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), msg, rowId);
            
            sa.ExecuteNonQuery(sqlF, connectionString);
        }

        public void SetComplete(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetComplete()");
            YSSqlAgentSQL sa = new YSSqlAgentSQL(logger);

            string sqlS = string.Format(SQL_STMT_UPDATE, "Complete",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Empty, rowId);
            
            sa.ExecuteNonQuery(sqlS, connectionString);
        }
    }
}
