﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace YSUtilities
{
    public class BankRes_PageInfo
    {
        public string ExcelInputFileName = string.Empty;
        public string ExcelOutputFileName = string.Empty;
        public string OutputDirectory = string.Empty;

        public string ReportName = string.Empty;
        public string PageName = string.Empty;

        public string Period = string.Empty;
        public string TotalLineText = string.Empty;
        public string SheetName = string.Empty;
        public string PreviousSheetName = string.Empty;

        public int FirstDataRow;
        public int DataHeaderRow;
        public int ColumnHeaderRow;

        public DateTime FirstDay = DateTime.MinValue;
        public DateTime LastDay = DateTime.MinValue;

    }
}
