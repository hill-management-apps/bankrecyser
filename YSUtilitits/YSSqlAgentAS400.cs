﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using YSUtilities;



namespace YSUtilities
{
    public class YSSqlAgentAS400
    {
        protected OleDbConnection AS400Connection = null;
        private YSLogger logger = null;

        public YSSqlAgentAS400(YSLogger sfLogger)
        {
            logger = sfLogger;
        }

        public DataSet ExecuteQuery(string sql)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            logger.LogMessage("Here in YSSqlAgentAS400.ExecuteQuery");

            try
            {
                AS400Connection = new OleDbConnection();
                AS400Connection.ConnectionString = "Provider=IBMDA400; Data Source=vader; User ID=mkoons; Password=Pwmk3579#; ";

                OleDbCommand CmdAS400 = new OleDbCommand(sql, AS400Connection);
                OleDbDataAdapter sqlAS400 = new OleDbDataAdapter() { SelectCommand = CmdAS400 };
                AS400Connection.Open();
                sqlAS400.Fill(dt);
                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in YSSqlAgentAS400.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
