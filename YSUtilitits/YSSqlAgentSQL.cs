﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace YSUtilities
{
    public class YSSqlAgentSQL
    {
        private YSLogger logger = null;

        public YSSqlAgentSQL(YSLogger sfLogger)
        {
            logger = sfLogger;
        }

        public DataSet ExecuteQuery(string sql, string connStr)
        {
            DataSet ds = new DataSet();
            logger.LogMessage("Here in YSSqlAgentSQL.ExecuteQuery");

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connStr);
                da.Fill(ds);

                return ds;
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in YSSqlAgentSQL.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void ExecuteNonQuery(string sql, string connString)
        {
            logger.LogMessage("Here in SqlAgent.ExecuteNonQuery");

            try
            {
                SqlConnection cn = new SqlConnection(connString);
                SqlCommand command = new SqlCommand(sql, cn);
                
                logger.LogMessage(string.Format("About to execute query: {0}", sql));
                logger.LogMessage(string.Format("On Connection: {0}", connString));

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
