﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace YSUtilities
{
    public sealed class YSLogger
    {
        private string outputPath = string.Empty;
        private string outputFile = string.Empty;
        public YSLogger(string outFile)
        {
            outputFile = outFile;
        }


        public void LogMessage(string msg)
        {
            File.AppendAllText(outputFile, string.Format("{0}\t{1}\n", DateTime.Now.ToLongTimeString(), msg));
            Console.WriteLine(string.Format("{0}\t{1}", DateTime.Now.ToLongTimeString(), msg));
        }
    }
}
