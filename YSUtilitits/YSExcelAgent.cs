﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using YSUtilities;

namespace YSUtilities
{
    public class YSExcelAgent
    {
        private Application app = null;
        private Workbook wb;
        private Worksheet ws;

        private YSLogger logger = null;
        private BankRes_PageInfo pageInfo = null;
        Dictionary<int, string> properties = new Dictionary<int, string>();
        string[] cols = { "B", "C", "D", "E", "F", "G", "H", "I", "J" };

        public YSExcelAgent(YSLogger sfLogger)
        {
            logger = sfLogger;
        }

        public void ApplyInitialFormat(BankRes_PageInfo pi)
        {
            try
            {
                pageInfo = pi;

                #region Create App, Workbook and Worksheet Objects
                // We only want to call ApplyInitialFormat on the Lockbox object
                // to Open the blank spreadsheet and fill in the date cells.
                // The rest of the objects will just write data into the sheets.
                if (pageInfo.SheetName == "Lockbox")
                {
                    // Create the  necessary junk for Excel to run
                    app = new ApplicationClass();       // App

                    // Run Excel VISIBLE
                    //app.DisplayAlerts = true;
                    //app.ScreenUpdating = true;
                    //app.Visible = true;
                    //app.UserControl = true;
                    //app.Interactive = true;

                    // run Excel INVISIBLE
                    app.DisplayAlerts = false;
                    app.ScreenUpdating = false;
                    app.Visible = false;
                    app.UserControl = false;
                    app.Interactive = false;

                    wb = app.Workbooks.Open(pageInfo.ExcelInputFileName);          // Workbook
                }
                #endregion

                // Set the active worksheet
                ws = (Worksheet)wb.Sheets[pageInfo.SheetName];

                if (ws.Name == "Lockbox")
                {
                    ws.Cells[3, 2] = pageInfo.FirstDay;
                    ws.Cells[3, 3] = pageInfo.LastDay;
                }
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in YSExcelAgent.ApplyInitialFormat: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void LoadDataRows(DataSet ds, Dictionary<string, bool> IncludedProperties)
        {
            try
            {
                #region Data Rows
                // Lay in the actual data
                int row = pageInfo.FirstDataRow;
                int col = 1;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        if (dc.ColumnName.ToLower() == "rdate")
                            ws.Cells[row, col++] = dr[dc.ColumnName];
                        else
                        {
                            if (IncludedProperties[dc.ColumnName.Substring(1)])
                                ws.Cells[row, col++] = dr[dc.ColumnName];
                            else
                                ws.Cells[row, col++] = 0;
                        }
                    }

                    row++;      // Next Row
                    col = 1;    // Reset Col to 1
                }
                #endregion

            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in YSExcelAgent.LoadDataRows: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void SaveAndClose(string filename)
        {
            try
            {
                logger.LogMessage("SFExcelAgent.SaveAndClose");
                logger.LogMessage(string.Format("About to save Excel File: {0}", filename));
                
                wb.SaveAs(filename, XlFileFormat.xlWorkbookDefault);
                wb.Close();
                app.Quit();

                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in YSExcelAgent.SaveAndClose: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
